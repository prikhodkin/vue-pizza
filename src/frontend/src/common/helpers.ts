const setDoughType = (item: string) => {
  switch (item) {
    case 'Толстое':
      return 'large'

    default:
      return 'light'
  }
}

const setPizzaSize = (item: string) => {
  switch (item) {
    case '32 см':
      return 'normal'

    case '45 см':
      return 'big'

    default:
      return 'small'
  }
}

const setSaucesType = (item: string) => {
  switch (item) {
    case 'Томатный':
      return 'tomato'

    default:
      return 'creamy'
  }
}

const setFillingFromImg = (pathToImg: string) => {
  return pathToImg.replace(/\/public\/img\/filling\/([\D]+)\.svg/, '$1')
}

export const formatDough = (arr: Dough[]) => {
  return arr.map((el) => {
    return {
      ...el,
      type: setDoughType(el.name)
    }
  })
}

export const formatSizes = (arr: Size[]) => {
  return arr.map((el) => {
    return {
      ...el,
      size: setPizzaSize(el.name)
    }
  })
}

export const formatSauces = (arr: Sauce[]) => {
  return arr.map((el) => {
    return {
      ...el,
      type: setSaucesType(el.name)
    }
  })
}

export const formatIngredients = (arr: Ingredient[]) => {
  return arr.map((el) => {
    return {
      ...el,
      filling: setFillingFromImg(el.image)
    }
  })
}
