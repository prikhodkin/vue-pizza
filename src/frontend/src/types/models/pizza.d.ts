interface Pizza {
  dough: Dough[]
  ingredients: Ingredient[]
  sauces: Sauce[]
  sizes: Size[]
}
