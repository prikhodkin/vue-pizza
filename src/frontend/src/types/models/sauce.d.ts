interface Sauce {
  id: number
  name: string
  price: number
  type?: string
}
