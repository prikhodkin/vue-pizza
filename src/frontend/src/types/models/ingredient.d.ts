interface Ingredient {
  id: number
  name: string
  image: string
  price: number
  filling?: string
  quantity?: number
}
