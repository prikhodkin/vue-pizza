interface Size {
  id: number
  name: string
  image: string
  multiplier: number
  size?: string
}
