interface OrderPizza {
  name: string,
  sauceId: number | null
  doughId: number | null
  sizeId: number | null
  quantity: number
  ingredients: {
    ingredientId: number | null
    quantity: number
  }[]
}
