interface Dough {
  id: number
  name: string
  image: string
  description: string
  price: number
  type?: string
}